require 'nokogiri'
require 'open-uri'
require_relative 'lunch_details'

URL="https://www.fi.issworld.com/palvelumme-service/ruokailupalvelut/ravintolat/tietokyla/opiskelijaravintola-ruokalista"

def get_lunch
  n = Nokogiri::HTML(open(URL))
  l = n.search("#maincontent_0_contentpage_content_1_title ~ div > table td")
  wno = Date.today.cweek
  g = l.reduce [] do |d, e|
    if e.content =~ /(vko|viikko) (\d+)/i
      wno = $2.to_i
      next d
    end
    if e.content =~ /l=laktoositon, m=maidoton/i
      next d
    end
    is_empty = e.content =~ /\A\p{Z}*\z/
    if is_empty or e.content =~ /L ?= ?laktoositon/i
      d
    else
      if e.child.name == "strong"
        [*d, [e.child.content.strip]]
      else
        [*d[0..-2], [*d[-1], e.content.strip]]
      end
    end
  end

  p g
  g.map do |e|
    LunchDetails.new(e[0], wno, e[1..-1])
  end
end

def get_upcoming_lunch
  lunches = get_lunch
  next_day = Date.today
  next_day += 1 if DateTime.now.hour >= 13
  lunches.detect do |l|
    l.date == next_day
  end
end

