CHAT_ID     = "@otalounas"
ADMINS      = ["50886815"]
ENVIRONMENT = ENV["ENV"]&.to_sym || :test
COMMENT     = ENV["COMMENT"]

require_relative 'get_lunch'
require_relative 'telegram_api'

begin
  if l = get_upcoming_lunch
    msg = [l.date_str, *l.content_emojified, COMMENT].compact.join "\n"
    telegram_send CHAT_ID, msg
  elsif Date.today.wday == 1 # Monday is usually the day when the list hasn't yet been updated
    ADMINS.each do |admin|
      telegram_send admin, "List hasn't been updated. Re-run manually."
    end
  end
rescue Exception => e
  msg = "Error: #{e.message}\n" +
    e.backtrace.map {|s| "  from #{s}\n"}.join
  puts msg
  ADMINS.each do |admin|
    telegram_send admin, msg
  end
end
