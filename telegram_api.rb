require 'json'
require 'net/http'

if ENVIRONMENT == :production
  BOT_TOKEN = File.read("secrets/bot_token").strip
else
  BOT_TOKEN = "_"
end

def telegram_send chat_id, text, **params
  params[:chat_id] = chat_id
  params[:text] = text
  uri = URI.parse "https://api.telegram.org/bot#{BOT_TOKEN}/sendMessage"
  if ENVIRONMENT == :production
    response = Net::HTTP.post uri, params.to_json, "Content-Type" => "application/json"
    unless response.is_a? Net::HTTPSuccess
      raise ArgumentError, "Telegram send failed: #{response.body}"
    end
  else
    puts "Would send message #{params.inspect}"
    puts uri.to_s
  end
end
