def biased_random_choice ary
  l = ary.length
  r = rand(l)+rand(l)-l+1
  r = -r-1 if r < 0
  ary[r]
end
