#!/bin/bash

. ~/.rvm/scripts/rvm

cd "$(dirname "$0")"
rvm use 2.6.3 > /dev/null
export ENV=production
bundle exec ruby send_lunch.rb
