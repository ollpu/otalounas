require_relative 'helper'

WDAYS=["Su", "Ma", "Ti", "Ke", "To", "Pe", "La"]
WDAYS_L=WDAYS.map(&:downcase)


class LunchDetails
  attr_accessor :date, :content
  def initialize human_day, week, content
    self.date = Date.commercial(Date.today.year, week, WDAYS_L.index(human_day[0...2].downcase))
    self.content = content
  end
  
  def date_str
    "#{WDAYS[date.wday]} #{date.strftime("%-d.%-m.%Y")}"
  end
  
  NORM_EMOJI="🍽🥓🥩🍗🍖🍔🍕🍝🍴"
  VEG_EMOJI="🥗🥕🍏🍎🍽🍐🍊🍋🍉🍍🥥🥝🍅🍆🥑🥦🥬🥒🌽🍴"
  EMOJI_MAP = {
    /pizza|pitsa/ => "🍕",
    /tortilla|burrito/ => "🌯",
    /taco/ => "🌮",
    /kebab/ => "🥙",
    /omena/ => "🍏🍎",
    /päärynä/ => "🍐",
    /appelsiini/ => "🍊",
    /sitruuna/ => "🍋",
    /meloni/ => "🍉",
    /ananas/ => "🍍",
    /kookos/ => "🥥",
    /kiivi/ => "🥝",
    /porkkana/ => "🥕",
    /tomaattikastike/ => "🥫",
    /munakoiso/ => "🍆",
    /avokado/ => "🥑",
    /parsakaali/ => "🥦",
    /kaali|pinaatti/ => "🥬",
    /pekoni/ => "🥓",
    /kesäkurpitsa/ => "🥒",
    /ohra/ => "🌾",
    /kurkku/ => "🥒",
    /chili|pippuri/ => "🌶",
    /bataatti/ => "🍠",
    /tofu/ => "🍢",
    /tomaatti/ => "🍅",
    /juusto/ => "🧀",
    /pasta|spagh?etti|makaroni|bolognese/ => "🍝",
    /nuudel/ => "🍜",
    /kala|lohi|hauki|kuha|muikku|siika|silakka/ => "🐟",
    /curry/ => "🍛",
    /possu/ => "🐖",
    /riisi/ => "🍚",
    /muna(?!koiso)/ => "🥚🍳",
    /maissi/ => "🌽",
    /peruna/ => "🥔",
    /basilika|oregano|yrtti/ => "🌿",
    /siipikarja|(?<!pork)kana(?!nmuna)|kalkkuna/ => "🍗",
    /liha/ => "🥩🍖",
    /maito/ => "🥛",
    /suola/ => "🧂",
    /salaatti/ => "🥗",
    /keitto/ => "🍲🥣",
    /leipä/ => "🍞🥖",
    /hapan/ => "🍋",
    /laatikko/ => "📦",
  }
  def self.get_emoji type, content
    content = content.downcase
    opts = ""
    EMOJI_MAP.each do |k, v|
      opts << v if content =~ k
    end
    if opts == ""
      opts = if type == :normal
        NORM_EMOJI
      else
        VEG_EMOJI
      end
    end
    biased_random_choice opts.chars
  end
  
  def content_emojified
    content.map do |c|
      if c.gsub! /KOTIRUOKA: ?/, ""
        c = "#{LunchDetails.get_emoji(:normal, c)} #{c}"
      elsif c.gsub! /KASVISRUOKA: ?/, ""
        c = "#{LunchDetails.get_emoji(:vegetarian, c)} #{c}"
      end
      c
    end
  end
  
  def to_json options=nil
    { date: date, content: content }.to_json options
  end
end
